/***************************************************
* Module name: main
*
* Copyright 2015 Hill-Rom
* All Rights Reserved.
*
*  The information contained herein is confidential
* property of Hill-Rom. The user, copying, transfer or
* disclosure of such information is prohibited except
* by express written agreement with Hill-Rom.
*
* First written on february 2015 by Jose Eslava.
*
* Module Description:
* \brief
* \details
*C'est mouais ltoto
*
*MAINMAINMAIN
*
*
*/

/***************************************************
*  Include section
***************************************************/
#include <stdio.h>
#include <string.h>

#include "defines_HR.h"
#include "tm4c1294kcpdt.h"
#include "sysctl.h"
#include "gpio.h"
#include "hw_memmap.h"
#include "scheduler/api_taskScheduler.h"
#include "scheduler/api_sysTick.h"
#include "MCO/mco.h"
#include "openbus/ddi_openbus.h"
#include "siderail/ddi_siderail.h"
#include "hand_pendant/ddi_handPendant.h"
#include "psmb/ddi_psmb.h"
#include "code_error/ddi_code_error.h"
#include "nurse_call/ddi_nursecall.h"
#include "night_light/ddi_nightlight.h"
#include "HILO_pedal/ddi_hilopedal.h"
#include "brake/ddi_brake.h"
#include "mec_CPR/ddi_mecCPR.h"
#include "sound_alerts/ddi_soundAlerts.h"
#include "HOB_Acc/ddi_HOB_acc.h"
#include "power_supply/ddi_powerSupply.h"
#include "diagnostic/ddi_io_diag.h"
#include "watchdog/ddi_wdt_config.h"
#include "eeprom/ddi_eeprom.h"
#include "uart/ddi_uart.h"
#include "SPI/ddi_spi.h"
#include "io_EXP/ddi_ioexp.h"
#include "siderail_position/ddi_siderailP.h"
#include "PPM/ddi_PPM.h"
#include "siderail_position/ddi_siderailP.h"
#include "scheduler/api_taskScheduler.h"
#include "app/app_spi.h"
#include "app/app_openbus.h"
#include "app/app_hob.h"
#include "app/app_Canopen.h"
#include "app/app_nightlight.h"
#include "app/app_psmb.h"
#include "app/app_brake.h"
#include "app/app_hilopedal.h"
#include "app/app_CPR.h"
#include "app/app_soundAlerts.h"
#include "app/app_diagnostic.h"
#include "app/app_initversion.h"
#include "app/app_nurse_call.h"
#include "app/app_wd.h"
#include "app/app_ICB_can.h"
#include "app/app_ioexp.h"
#include "app/app_MCB.h"
#include "app/app_powerSupply.h"
#include "app/app_PPM.h"
#include "app/app_error.h"
#include "app/app_NotifLed.h"
#include "app_alert.h"
//for trace
#include "pin_map.h"

#ifdef DEBUG_MAIN
#define DEBUG_TRACE_MAIN_LOG(...) UARTprintf(__VA_ARGS__)
#else
#define DEBUG_TRACE_MAIN_LOG(...)
#endif

// ENABLE TRACE FOR DEBUG ONLY
#ifdef DEBUG_ON_OFF
#include "hw_i2c.h"
#define DEBUG_TRACE_ON_OFF_LOG(...) UARTprintf(__VA_ARGS__)
#else
#define DEBUG_TRACE_ON_OFF_LOG(...)
#endif

// ENABLE TRACE FOR DEBUG ON ADS1241 ONLY
#ifdef DEBUG_ADS1241
#define DEBUG_TRACE_ADS1241_LOG(...) UARTprintf(__VA_ARGS__)
#else
#define DEBUG_TRACE_ADS1241_LOG(...)
#endif

uint32_t HR_Sys_Clock_Freq;

void app_task_NBH ( void )
{
	App_nightlight_Task (  );
	App_brake_Task (  );
	App_hilopedal_Task (  );

	if ( !AC_MODE )
	{
		Ddi_code_error_Save ( );
	}
}

//*****************************************************************************
//
//
//
//*****************************************************************************
int main ( void )
  {
	uint32_t i;
	uint32_t temp=0;

	openBusNotReady = 0xff;

	// Setup the system clock to run at 80 Mhz from PLL with crystal reference
	HR_Sys_Clock_Freq = SysCtlClockFreqSet ( SYSCTL_OSC_MAIN | SYSCTL_USE_PLL | SYSCTL_XTAL_25MHZ | SYSCTL_CFG_VCO_480, 80000000 );

	Ddi_uart_Init ( );                      // init uart driver

	DEBUG_TRACE_MAIN_LOG ( "####### MAIN\n" );
	DEBUG_TRACE_MAIN_LOG ( "    start init\n" );
	DEBUG_TRACE_ON_OFF_LOG( " **************************************** Main Start ***************************************************. %d\n", __LINE__);
	DEBUG_TRACE_ADS1241_LOG( " **************************************** Main Start ***************************************************.");

	Ddi_wdt_init ( );                       // init watch dog driver

	Ddi_eeprom_init ( );                    // init EEprom driver
	App_Init_Error();                       // init ICB error CANOpen model
	Ddi_eeprom_initBedType();								// Read the Bed configuration from EEPROM

	temp = Ddi_eeprom_read_HRP ( );
	if ( ( temp == 0x00 ) ||
		( temp == 0xffffffff ) )
	{
		check_And_Add_In_Error_List ( ID_ICB, FAILURE_NO_HRP, PRIORITY_HIGH );
	}
	else
	{
		check_If_Error_Was_Active_And_Unset ( ID_ICB, FAILURE_NO_HRP );
	}
	temp = 0;

	// init PSMB driver if PSM is available
	if ( bed_config.bits.PSMB )
	{
		SysCtlPeripheralEnable ( PORT_OUT_PSMB_POWER_ON );
		GPIOPinTypeGPIOOutput ( PORT_OUT_PSMB_POWER_ON, PIN_OUT_PSMB_POWER_ON );
		// Power on 12.25V and 29.2V ( chargers ) on PSMB
		GPIOPinWrite ( PORT_OUT_PSMB_POWER_ON, PIN_OUT_PSMB_POWER_ON,PIN_OUT_PSMB_POWER_ON );
	}

	Ddi_openbus_prepare_init ( );           // realse power request
	Ddi_ioexp_Init ( );                     // init IO expender driver
	Ddi_io_InitDiagnostModule ( );          // init diag module driver
	Ddi_io_InitPowerSupply();               // set as output GPIOs and set to ON 8.25V


	i = 0;                                  // add wait state else crash
	while ( i++ < 2000 );                   // when remote cmd plugged

	//
	// Temporization which allow to power on complety the external peripheral (ADC, IO expander for example)
	// SysCtlDelay(n) performs n loop.
	// For each loop, 3 instructions. Period for each loop = 3* (1/freq osc), 1 CLK per instruction
	// Temporization = 300000*(3*1/80MHz) = 11 ms
	SysCtlDelay(300000);

	Ddi_io_Activate8VPowerSupplySPI_LR_Siderail_handPendant();  //set pin 8.25V_SP1_EN to 1
	Ddi_SPI_LR_Siderail_Hand_Pendant_Init ( );

	Ddi_io_Activate8VPowerSupplySPI_LR_Siderail_footPendant();  //set pin 8.25V_SP2_EN to 1
	Ddi_SPI_LR_Siderail_Foot_Pendant_Init ( );

	// Power On and Init PPM pendant if PPM module existing
	if ( bed_config.bits.PPM_Control )
	{
		// Set Pin 8.25V_SPI3_EN to 1, power supply activation for PPM Pendant
		Ddi_io_Activate8VPowerSupplySPI_PPM_Control ( );
		Ddi_SPI_PPM_Control_Init ( );
	}

	// In the function open bus init, the software initializes the interruption Timer.
	// In this interruption init, the software copy interruption vector in RAM memory (by lib TI)
	// Before, the interruption vector used is the vector of bootloader.
	// It's not possible to change the vector of interruption after the start if the vector is not copied in RAM.
	// For this, it's important to set this function open bus init at this place to allow to copy interruption vector in RAM.
	// In other case, all interruption are not called (because interruption vector used is this of the bootloader software.
	// Do not change the call of open bus init without know the correct behaviour of interruption vector.
	Ddi_openbus_init ( );                   // init the open bus driver
	App_Activate8VPowerSupply();

	if ( bed_config.bits.NURSECALL )
	{
		Ddi_io_InitNurseCall ( );           // Init Nursecall driver
	}

	Ddi_io_InitNightLight ( );              // Init NightLight driver
	Ddi_io_InitHiloPedal ( );               // Init HiloPedal driver
	Ddi_io_InitBrake ( );                   // Init brake driver

	// Init Mecanical CPR driver for BED different from HR900+
	if ( bed_config.bits.siderailControlAccella )
	{
		Ddi_io_InitMecCPR ( );
	}

	// Init HOB driver if existing
	if ( bed_config.bits.HOB )
	{
		Ddi_HOB_Init ( );
	}

	Ddi_io_InitSoundAlerts ( );             // Init SoundAlert driver

	// Read EEPROM to find if an error was present before the last power off
	error_flag = Ddi_eeprom_read_err_flag();
	if ( bed_config.bits.PSMB )
	{
		temp= Ddi_eeprom_read_batt_status();
	}
	//CAN peripherics and CANOpen stack Initialization
	MCOUSER_ResetCommunication ( );

	//Init diagnostic model with values stored in EEPROM
	App_Init_EEPROM_DiagModel ( );
	App_Init_ICB_CANOpen_Model ( );

	// Init the PPM driver if PPM exists
	if ( bed_config.bits.PPM || bed_config.bits.SCALE )
	{
		// Init PPM
		initPPM ( );
	}

	// Init I2C and ADC interfaces for PSMB driver
	if ( bed_config.bits.PSMB ) //be carefull of the ramp up of the regulator at startup
	{
		DEBUG_TRACE_ON_OFF_LOG( "I2C slave be : 0x%08x\n", * ( uint32_t * ) ( I2C0_BASE + I2C_O_MSA  ) );
		DEBUG_TRACE_ON_OFF_LOG( "I2C status be : 0x%08x\n", * ( uint32_t * ) ( I2C0_BASE + I2C_O_MCS ) );
		Ddi_init_psmb ( );
		DEBUG_TRACE_ON_OFF_LOG( "I2C slave af : 0x%08x\n", * ( uint32_t * ) ( I2C0_BASE + I2C_O_MSA  ) );
		DEBUG_TRACE_ON_OFF_LOG( "I2C status af : 0x%08x\n", * ( uint32_t * ) ( I2C0_BASE + I2C_O_MCS ) );
		PSMB_Battery_Status_Model.blocks.STATUS_MATTRESS = (temp)&(0x000000FF);
		PSMB_Battery_Status_Model.blocks.STATUS_BATTERIES = (((temp)&(0x0000FF00))>>8);
		PSMB_Battery_Status_Model.blocks.CHG_OCCURING = (((temp)&(0x00FF0000))>>16);
		PSMB_Battery_Status_Previous_Model.blocks.STATUS_MATTRESS = (temp)&(0x000000FF);
		PSMB_Battery_Status_Previous_Model.blocks.STATUS_BATTERIES = (((temp)&(0x0000FF00))>>8);
		PSMB_Battery_Status_Previous_Model.blocks.CHG_OCCURING = (((temp)&(0x00FF0000))>>16);
	}


#ifdef DEBUG_ON_OFF
	{
		// check configuration I2C
		Ddi_I2C_Receive_PSMB((uint32_t*)(&input_config),(uint8_t)ADDR_PSMB,(uint8_t)CONF_PORT0_PSMB);
		SysCtlDelay(30000); // wait at least 1 ms (for debug)
		Ddi_I2C_Receive_PSMB((uint32_t*)(&output_config),(uint8_t)ADDR_PSMB,(uint8_t)CONF_PORT1_PSMB);
		DEBUG_TRACE_ON_OFF_LOG("I2C configuration after init, input: 0x%02x, output : 0x%02x\n", input_config,output_config);
	}
#endif

	// Init Siderail Position driver
	if ( bed_config.bits.SIDERAIL_POSITION == TRUE )
	{
		Ddi_siderailP_Init ();
	}

	// Enable processor interrupts.
	IntMasterEnable ( );

	//SysTick Init
	Api_sysTick_SysTickInit ( );

	// Update watchdog Pin
	App_wd_Task();

	DEBUG_TRACE_MAIN_LOG ( "    create task\n" );

	// Tasks creation
	Api_create_task ( TASK_PRIORITY_0, TASK_EXEC_PER_10_MILI_SEC, App_SPI_Task, "App_SPI_Task", 0 ); //Siderail task
	Api_create_task ( TASK_PRIORITY_1, TASK_EXEC_PER_100_MILI_SEC, App_ManagePower_Task, "App_ManagePower_Task", 0 ); // Power Management Task

	Api_create_task ( TASK_PRIORITY_2, TASK_EXEC_PER_100_MILI_SEC, AppIoExp_MainTask, "AppIoExp_MainTask", 0 );//IOexp task
	Api_create_task ( TASK_PRIORITY_3, TASK_EXEC_PER_100_MILI_SEC, App_Openbus_Task, "App_Openbus_Task", 0 );//Openbus tasks
	Api_create_task ( TASK_PRIORITY_6, TASK_EXEC_PER_100_MILI_SEC, app_task_NBH, "NightLight Brake HiloPedal", 0 );//Night light task
	
	if ( bed_config.bits.HOB )
	{
		Api_create_task ( TASK_PRIORITY_7, TASK_EXEC_PER_10_MILI_SEC, App_HOB_Task, "App_HOB_Task", 0 ); //HOB task
	}
	else
	{
		check_If_Error_Was_Active_And_Unset ( ID_ICB, HOB_ACTUATOR_COMMUNICATION );
		HOB_ACTUATOR_COMMUNICATION_NB_ERR_OCC = 0;
		check_If_Error_Was_Active_And_Unset ( ID_ICB, HOB_RAW_VALUE_OUTOFRANGE );
		HOB_RAW_VALUE_OUTOFRANGE_NB_ERR_OCC = 0;
		check_If_Error_Was_Active_And_Unset ( ID_ICB, HOB_OUT_OF_RANGE );
		HOB_OUT_OF_RANGE_NB_ERR_OCC = 0;
	}

	if ( bed_config.bits.siderailControlAccella )
	{
		Api_create_task ( TASK_PRIORITY_10, TASK_EXEC_PER_10_MILI_SEC, App_CPR_Task, "App_CPR_Task", 0 ); //Mechanical CPR task
	}
	else
	{
		check_If_Error_Was_Active_And_Unset ( ID_ICB, CMD_MECHANICAL_CPR_TIMEOUT );
		check_If_Error_Was_Active_And_Unset ( ID_ICB, CMD_MECHANICAL_CPR_ERROR );
	}

	Api_create_task ( TASK_PRIORITY_11, TASK_EXEC_PER_100_MILI_SEC, App_soundAlerts_Task, "App_soundAlerts_Task", 0 );//Sound alerts task
	Api_create_task ( TASK_PRIORITY_12, TASK_EXEC_PER_100_MILI_SEC, App_diagnostic_Task, "App_diagnostic_Task", 0 );//Diagnostic Task

	if ( bed_config.bits.PPM || bed_config.bits.SCALE )
	{
		Api_create_task ( TASK_PRIORITY_14, TASK_EXEC_PER_100_MILI_SEC, App_PPM_Task, "App_PPM_Task", 0 );//PPM Task
	}

	Api_create_task ( TASK_PRIORITY_15, TASK_EXEC_PER_10_MILI_SEC, App_CanOpen_CheckTxQueue_Task, "App_CanOpen_CheckTxQueue_Task", 0 ); //CAnOpen tasks
	Api_create_task ( TASK_PRIORITY_16, TASK_EXEC_PER_10_MILI_SEC, App_CanOpen_ProcessRxQueue_Task, "App_CanOpen_ProcessRxQueue_Task", 0 );
	Api_create_task ( TASK_PRIORITY_17, TASK_EXEC_PER_10_MILI_SEC /* TASK_EXEC_PER_100_MILI_SEC JMA */, App_CanOpen_UpdatePDOs_Task, "App_CanOpen_UpdatePDOs_Task", 0 );
	Api_create_task ( TASK_PRIORITY_24, TASK_EXEC_PER_100_MILI_SEC, App_InitVersion_Task, "App_InitVersion_Task", 0);

	if ( bed_config.bits.NURSECALL )
	{
		Api_create_task ( TASK_PRIORITY_18, TASK_EXEC_PER_100_MILI_SEC, App_nurse_callTask, "App_nurse_callTask", 0 );//Nurse call task
	}

	if ( bed_config.bits.MATTRESS )
	{
		Api_create_task ( TASK_PRIORITY_20, TASK_EXEC_PER_100_MILI_SEC, App_MCB_Task, "App_MCB_Task", 0 ); //Nurse call tasks
	}

	Api_create_task ( TASK_PRIORITY_21, TASK_EXEC_PER_100_MILI_SEC, App_CheckForTurningOffFlagTask, "App_CheckForTurningOffFlagTask", 0 ); //Secure Turning off task

	Api_create_task ( TASK_PRIORITY_23, TASK_EXEC_PER_100_MILI_SEC, App_wd_Task, "toto qui dit", 0 ); //Watchdog task

	if ( bed_config.bits.GUI )
	{
		Api_create_task ( TASK_PRIORITY_25, TASK_EXEC_PER_100_MILI_SEC, App_GetTime_Task, "App_GetTime_Task", 0 ); //Get time HIB
	}

	DEBUG_TRACE_MAIN_LOG ( "    start execution\n" );
	while ( 1 )
	{
		Api_schedule_tasks ();
	}
}

